## База данних
###### Update BUILDER
```csharp
    private bool update() {
       SqlUpdateBuilder cmd = SqlClient.Main.CreateUpdateBuilder();

        cmd.Table.Name = "TABLE";
        //заповнюємо поля: хто створив, редагував, коли створили і редагували
        cmd.AddSignature = true;
		
        cmd.Set.SetValue("field1_in_db", "value1");
        cmd.Set.SetValue("field2_in_db", "value2");
		
        cmd.Where.CommandText = "ID = @ID";
        cmd.Where.Parameters.Add("ID", "value");
		
        return cmd.Exec();
    }
```
###### Insert BUILDER
```csharp
    private bool update() {
        SqlInsertBuilder cmd = SqlClient.Main.CreateInsertBuilder();
        cmd.TableName = "TABLE";
        //заповнюємо поля: хто створив, редагував, коли створили і редагували
        cmd.AddSignature = true;
		
        cmd.AddValue("field1_in_db", "value1");
        cmd.AddValue("field2_in_db", "value2");

        return cmd.Exec();
    }
```
###### Simple Select BUILDER
```csharp
    private bool getData() {
        SqlSelectBuilder cmd = SqlClient.Main.CreateSelectBuilder();
        cmd.From.Table.Name = TABLE;

        cmd.Fields.Add(TABLE, "field1");
        cmd.Fields.Add(TABLE, "field2");

        cmd.Where.CommandText = "ID = @ID";
        cmd.Where.Parameters.Add("ID", "value");

       var data = cmd.GetCommand().ExecObject(
            new {
                field1 = string.Empty,
                field2 = string.Empty
            }
        );			
        return data;
    }
```
## Форми
###### Створення користувацької кнопки
```csharp
protected override void ScreenWhen()
    {
        {
            var btn = base.ToolbarButtons.Add("keyBtn", "NAME BTN");
            btn.Image = "vers";
            btn.Click += () =>
            {
                //Логіка при натисканні
                InfoManager.MessageBox("Click this BTN");
            };
        }
    }
```
###### Очищення поля в формі
```csharp
ClearValues(_input);
```
###### Режим редагування
```csharp
    //повертає true якщо режим редагування = додаванню
    private bool isEditModeAdd()
    {
        return EditMode == EditMode.Add;
    } 
    //повертає true якщо режим редагування = редагування
    private bool isEditModeEdit()
    {
        return EditMode == EditMode.Edit;
    } 
    
    //значення EditMode
     public enum EditMode
    {
        None = 0,
        View = 1,
        Edit = 2,
        Add = 3,
        Sample = 4,
        Delete = 5
    }
```
###### Заповнення інформацією GRID елементу в формі
```csharp
string sqlQuery = "SELECT id, name FROM TABLE WHERE id = 1";
var sqlCmd = SqlClient.Main.CreateCommand(sqlQuery);
string localTable = sqlCmd.LoadToLocalDb();
//заносимо локальну таблицю в грід
_grid.InnerControl.DataTable = new GridCursor(localTable);

//дадаємо колонки у вигляд GRID
_grid.InnerControl.Columns.Add("id", "ID");
_grid.InnerControl.Columns.Add("name", "Імя");
```
###### Додавання колонок в GRID
```csharp
/**
    NAME_FIElD_IN_LOCAL_DB - імя поля в локальній таблиці
    NAME__COL_IN_GRID - імя колонки в GRID 
**/
_grid.InnerControl.Columns.Add("NAME_FIElD_IN_LOCAL_DB", "NAME__COL_IN_GRID");
```

###### Додавання кноки в нижню панель GRID
```csharp
var btn = _grid.InnerControl.ToolBarButtons.Add("create", "Добавити");
btn.Click += () =>
{
    //логіка кнопки
};

```
###### Отримати кількість записів в GRID
```csharp
_grid.InnerControl.DataTable.RecCount
```
###### Видалення запису з GRID
```csharp
_grid.InnerControl.DataTable.DeleteRecord();
```

## RDL звіти
###### Додавання RDL параметрів до звіту
```csharp
protected override IEnumerable<ITnet2.Server.Reporting.RdlReport.ReportParameter> GetAdditionalRdlParameters()
      {  
	  return new List<ITnet2.Server.Reporting.RdlReport.ReportParameter> {
    			new ITnet2.Server.Reporting.RdlReport.ReportParameter<string>("Title", "Звіт")
    		};
      }
```
###### Додавання DataSource в RDL звіт
```csharp
 protected override IEnumerable<RdlReport.DataSource> GetAdditionalRdlDataSources()
    {
        //наш запит до БД
        string q = "SELECT * FROM TABLE";
        //отримуємо локальну БД з нашого запиту
        string ltbl = SqlClient.Main.CreateCommand(q).LoadToLocalDb();
		//додаємо  dataSource до нашого rdl-звіта
        RdlReport.DataSource dataSource = new RdlReport.DataSource("OBJECT", ltbl);
        return new[] { dataSource };
    }
```
###### Додавання фільтра перед друкування. Приклад фільтр по даті.
```csharp
    private DateTime _dateFrom;
    private DateTime _dateTo;

protected override bool BeforePrint(string localTableName)
{
    if (base.BeforePrint(localTableName))
    {
        _dateTo = Settings.DateTimeSql;
        _dateFrom = _dateTo.AddMonths(-1);

        if (!CommonDialogs.PeriodPicker(ref _dateFrom, ref _dateTo, new CommonDialogs.PeriodPickerParameters())) return false;
    }
    return base.BeforePrint(localTableName);
}
```
###### Друк звіта за допомогою кода
```csharp
Cursor.Reports.Print("id_report");
```
## Інтерфейси
###### Виклик інтерфейсу і передача параметрів
```csharp
/**
    Зі сторони де викликається інтерфейс
**/

DataEditor.StartInfo INTERFACE = new DataEditor.StartInfo("NAME_INTERFACE");
//Create a custom property in the interface.
INTERFACE.CustomProperties.Add("NAME_CUSTOM_PROPERTIES", "VALUE_CUSTOM_PROPERTIES");
DataEditor.Call(INTERFACE);

/**
    В курсорі
    Приклад для фільтру
**/
public override SqlCmdText GetSqlFilter()
{
    //get custom property
    var valueCustomProperty = Dialog.Properties.Get<int>("NAME_CUSTOM_PROPERTIES");
    //filter
    SqlCmdText cmdt = new SqlCmdText(string.Format(@" _NAME_FIELD = {0}", valueCustomProperty));
    return cmdt;
}
```

## Загальне
###### Отримання данних з вибраної стрічки в курсорі
```csharp
private int getValueField(){
    return base.Cursor.CurrentRow.GetFieldValue<int>("name_field"); 
}
// or
private string getValueField(){
    return Cursor.Dialog.MainCursor.GetFieldValue<string>("name_field");
}
```
###### Отримати код групи користувача
```csharp
    Settings.Environment.ItGroupCode;
```
###### Автозаповнення Перевірка орфограцфії 
```csharp
    [https://gitlab.com/maxim.kohanskij/snippets_it_enterprice/blob/master/img/viber_image_2018-06-20___14.37.23.jpg]('Зображення')
```
