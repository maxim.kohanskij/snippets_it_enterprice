/**
    Приклад Grid-а у формі з додавання, редагуванням і видаленням.
    GRUD реалізовано в ручну без допомоги IT-ентерпрайза
    Приклад використувується у проекті ДЕІ в основній формі
_____________________________________________________
*/



/**
    Код для форми де знаходиться самий Грід
*/

 string sqlQueryGetListObligations = string.Format(
                                            "select ID, DATE_COMPL from _DEI_OBL where ID_ST IN ({0})",
                                            getSqlPartiesContract("ID", idContarct)  //фільтрація зобов'язань по сторонам договру
            );
            //отримуємо локальну таблицю Зобов'язань
            var sqlCmdObligations = SqlClient.Main.CreateCommand(sqlQueryGetListObligations);
            string localTableNameObligations = sqlCmdObligations.LoadToLocalDb();
            //заносимо локальну таблицю в грід Зобов'язання
            _obligations.InnerControl.DataTable = new GridCursor(localTableNameObligations);
            //додаємо потрібні поля в грід Зобов'язання
            _obligations.InnerControl.Columns.Add("ID", "№");
            _obligations.InnerControl.Columns.Add("DATE_COMPL", "Дата виконання");


            _obligations.InnerControl.ShowExportButton = true; //вмикаємо кнопку експорта
            _obligations.InnerControl.ShowInstrumentalPanel = true; //вмикаємо інструментальну панель 

            var btnCreat_obligation = _obligations.InnerControl.ToolBarButtons.Add("create", "Добавити");//кнопка в нижній панелі

            btnCreat_obligation.Click += () => //логика кнопки додавання Зобов'язання
            {
                InputForm.Result deiObl = InputForm.Activate("_DEIOBL2", new Dictionary<string, object> //визиваємо форму "зобов'зання"
                                                                    {
                                                                        {"ID_OBLIGATION", "NULL"},//передаємо значення NULL, зазначаючи що це додавання
                                                                        {"ID_ST", _partiescontract.InnerControl.DataTable.GetFieldValue<int>("ID")}//передаємо індентифікатор сторони договру
                                                                    });
                if (deiObl.Success == true) _obligations.InnerControl.DataTable = this.updateGrid(sqlQueryGetListObligations); //оновлення гріда з БД, якщо форма завершилася успіхом
            };

            var btnEdit_obligation = _obligations.InnerControl.ToolBarButtons.Add("edit", "Редагувати");//кнопка редагування

            btnEdit_obligation.Click += () => // логіка для кнопки редагування
            {
                InputForm.Result deiObl = InputForm.Activate("_DEIOBL2", new Dictionary<string, object> //активація форми Зобов'язання
                                                                    {
                                                                        {"ID_OBLIGATION", _obligations.InnerControl.DataTable.GetFieldValue<string>("ID")} //передаю індинтифікатор зобов'язання
                                                                    });
                if (deiObl.Success == true) _obligations.InnerControl.DataTable = this.updateGrid(sqlQueryGetListObligations); //оновлення гріда з БД, якщо форма завершилася успіхом
            };

            var btnDelete_obligation = _obligations.InnerControl.ToolBarButtons.Add("delete", "Видалити");//кнопка видалення

            btnDelete_obligation.Click += () => //логіка кнопки видалення
            {
                InputForm.Result mainYN = InputForm.Activate("_MAIN_YN", new Dictionary<string, object> 
                                                                        { 
                                                                            {"QUESTION_SOURCE", "Ви впевнені що хочете видалити зобов'язання з ID: " + _obligations.InnerControl.DataTable.GetFieldValue<string>("ID") + "?"}
                                                                        });
                if (mainYN.Success == true)
                {
                    SqlClient.Main.CreateCommand(string.Format("delete from _DEI_OBL where ID = {0}", _obligations.InnerControl.DataTable.GetFieldValue<string>("ID"))).ExecNonQuery();
                    _obligations.InnerControl.DataTable.DeleteRecord();
                }
            };
/**
     Код для форми де знаходиться самий Грід
*/

/**
    Код форми редагування-додавання нового item
*/

    protected override void ScreenWhen()
    {
        _id_Obligation.Visible = false; // системне поле для Ід зобовязання
        if (_id_Obligation.Value != "NULL") // якщо там не НУЛЛ то ми витягуємо данні по ід зобовязання
        {
            //запит для отримання данних по одному зобовязанню
            var sqlSelectCmd = SqlClient.Main.CreateCommand(string.Format(@"select ID_ST, CONTROLLER, DATE_COMPL, SUM_DOG, PERFORMER, DATE_CONTR, NOTATE
                                                                            from _DEI_OBL 
                                                                            where ID = '{0}'", _id_Obligation.Value));
            //отримання данних
            var obligation = sqlSelectCmd.ExecObject( //get a collection
              new
              {
                  id_st = int.MinValue,
                  controller = string.Empty,
                  date_compl = System.DateTime.MinValue,
                  sum_dog = string.Empty,
                  performer = string.Empty,
                  date_contr = System.DateTime.MinValue,
                  notate = string.Empty
              });
            // вставляємо отриманні данні в потрібні поля
            _id_St.Value = obligation.id_st;
            _controller.Value = obligation.controller;
            _date_Compl.Value = obligation.date_compl;
            _sum_Dog.Value = obligation.sum_dog;
            _performer.Value = obligation.performer;
            _date_Contr.Value = obligation.date_contr;
            _notate.Value = obligation.notate;

        }
        else {
            if (!string.IsNullOrEmpty(_id_St.Value.ToString()))
            {
                SqlCmd sqlDateControl = SqlClient.Main.CreateCommand("select isnull(convert(varchar(20),_dei_db.date_contr, 104), '') from _dei_obl join _dei_st on _dei_st.id = _dei_obl.id_st join _dei_db on _dei_db.id = _dei_st.id_dog where _dei_obl.id_st=@ID_ST");
                sqlDateControl.Parameters.Add("ID_ST", _id_St.Value);
                _date_Contr.Value = sqlDateControl.ExecScalar<System.DateTime>();
            }
        }
        base.ScreenWhen();
    }
    protected override bool ScreenValid()
    {
        //якщо поле Ід в нас НУлл або пусте то ми додаємо нове зобовязання
        if (_id_Obligation.Value == "NULL" || string.IsNullOrEmpty(_id_Obligation.Value))
        {
            var sqlCmd = SqlClient.Main.CreateCommand(
                     string.Format("insert into _DEI_OBL (ID_ST, CONTROLLER, DATE_COMPL, SUM_DOG, PERFORMER, DATE_CONTR, NOTATE) values ('{0}','{1}', {2},'{3}','{4}',{5},'{6}')",
                     _id_St.Value, _controller.Value, convertDataSql(_date_Compl.Value), _sum_Dog.Value, _performer.Value, convertDataSql(_date_Contr.Value), _notate.Value)
                 );
            sqlCmd.ExecNonQuery();
        }
        else //якщо не Нулл то оновляєм існуюче зобовязання
        {
            var checkedIdPartyContract = SqlClient.Main.CreateCommand(
                             string.Format("select count(ID) from _DEI_OBL where ID = {0}", _id_Obligation.Value)
                             ).ExecScalar<int>(); // отримуємо данні чи є таке зобовязання
            // перевіряємо на існування зобовязання і якщо його немає, то видаємо помилку
            if (checkedIdPartyContract == 0) InfoManager.MessageBox("Не вірне значення ID зобов'язання. Зверніться до адміністратора.");
            //оновляємо данні в таблиці
            var sqlCmd = SqlClient.Main.CreateCommand(
                     string.Format("update _DEI_OBL set ID_ST = '{0}', CONTROLLER = '{1}', DATE_COMPL = {2}, SUM_DOG = '{3}', PERFORMER = '{4}', DATE_CONTR = {5}, NOTATE = '{6}' where ID = {7}",
                    _id_St.Value.ToString(), _controller.Value, convertDataSql(_date_Compl.Value), _sum_Dog.Value, _performer.Value, convertDataSql(_date_Contr.Value), _notate.Value, _id_Obligation.Value)
                 );
            sqlCmd.ExecNonQuery();
        }
        return base.ScreenValid();
    }

    // функція для конвертування дати в формати sql
     protected static string convertDataSql(System.DateTime dateTime)
     {
         return (dateTime.ToString() == "01.01.0001 0:00:00") ? "null" : "convert(datetime,'" + dateTime.ToString() + "', 104)";
     }
 /**
    Кінець Код форми редагування-додавання нового item
*/
     