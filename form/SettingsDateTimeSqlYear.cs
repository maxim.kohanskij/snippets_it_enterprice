public class NAME_FORM : _Km_TrBase
{
	private int this_year;

    protected override void ScreenWhen()
    {
		     this_year = Settings.DateTimeSql.Year;
            if(!CommonDialogs.YearPicker(ref this_year))
				return false;     

        base.ScreenWhen();
    }
}