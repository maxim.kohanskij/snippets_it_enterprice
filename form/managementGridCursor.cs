public class NAME_FORM : _Km_TrBase
{
    protected override void ScreenWhen()
    {
		int unid = Cursor.CurrentRow.GetFieldValue<int>("UNDLRK");
        string FAM = _fam.Value;

        var sqlCmd = SqlClient.Main.CreateCommand(
            string.Format("SELECT d.NDM, d.FAM FROM DLRKS d WHERE d.kdlts = '45' AND d.FAM = '{0}' AND  d.UNDLRK != {1}", FAM, unid)
            );
        string localTableName = sqlCmd.LoadToLocalDb();


        _lastrequest.InnerControl.DataTable = new GridCursor(localTableName);

        if (_lastrequest.InnerControl.DataTable.RecCount == 0)
        {
            _var6.Visible = false;
            return false;
        }
        _lastrequest.InnerControl.Columns.Add("NDM", "№");
        _lastrequest.InnerControl.Columns.Add("FAM", "П.І.Б");

        base.ScreenWhen();
    }
}