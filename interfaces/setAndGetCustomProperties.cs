    
	//class FORM
	public class NAME_FORM : _Km_TrBase
{
    protected override void ScreenWhen()
    {
		{
			//code for the interface call button, where we will read the property
            var btn = base.ToolbarButtons.Add("key", "NAME BTN");
            btn.Image = "vers";
            btn.Click += () =>
            {
				
				//NAME_INTERFACE - name interface
				DataEditor.StartInfo INTERFACE = new DataEditor.StartInfo("NAME_INTERFACE");
				//Create a custom property in the interface.
				//NAME_CUSTOM_PROPERTIES - name custom property
				//VALUE_CUSTOM_PROPERTIES - value custom property
                INTERFACE.CustomProperties.Add("NAME_CUSTOM_PROPERTIES", "VALUE_CUSTOM_PROPERTIES");
                DataEditor.Call(INTERFACE);
            };
        }

        base.ScreenWhen();

	}
	
	
	//class CURSOR
	public class NAME_CURSOR : DataEditorBusinessLogic.CursorLogic
{
	//method GetSqlFilter for cursor
    public override SqlCmdText GetSqlFilter()
    {
		/*
		* Example use fileter and custom property
		*/
		//get custom property
		//NAME_CUSTOM_PROPERTIES - name custom property
        var valueCustomProperty = Dialog.Properties.Get<int>("NAME_CUSTOM_PROPERTIES");
		//filter
        SqlCmdText cmdt = new SqlCmdText(string.Format(@" _NAME_FIELD = {0}", valueCustomProperty));
        return cmdt;
    }




}