using System.Collections.Generic;
using ITnet2.Server.Reporting;
using ITnet2.Server.Data;
using ITnet2.Server.Session;
using System.ComponentModel;
using ITnet2.Server.Dialogs;

public class BusinessLogic : BusinessLogicBase
{
    private DateTime _dateFrom;
    private DateTime _dateTo;


    protected override bool BeforePrint(string localTableName)
    {
        if (base.BeforePrint(localTableName))
        {
            _dateTo = Settings.DateTimeSql;
            _dateFrom = _dateTo.AddMonths(-1);

            if (!CommonDialogs.PeriodPicker(ref _dateFrom, ref _dateTo, new CommonDialogs.PeriodPickerParameters())) return false;
        }
        return base.BeforePrint(localTableName);
    }
}