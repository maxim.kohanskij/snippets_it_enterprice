 public class BusinessLogic : BusinessLogicBase
{
    private DateTime _dateFrom;
    private DateTime _dateTo;
    
      protected override IEnumerable<ITnet2.Server.Reporting.RdlReport.ReportParameter> GetAdditionalRdlParameters()
      {  return new List<ITnet2.Server.Reporting.RdlReport.ReportParameter> {
    			new ITnet2.Server.Reporting.RdlReport.ReportParameter<string>("DateFrom", Text.Convert(_dateFrom.Date, Text.DateTimeView.Date)),
    			new ITnet2.Server.Reporting.RdlReport.ReportParameter<string>("DateTo", Text.Convert(_dateTo.Date, Text.DateTimeView.Date)),
    		};
      }
  }